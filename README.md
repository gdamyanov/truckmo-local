# TRUCKMO

## Introduction

This repository guides you in setting up `TRUCKMO` locally with the help of `Vagrant`.

## Key Facts

Customer: [TRUCKMO](http://www.truckmo.com/)

Product Owner / Project Manager: [Christioph Batik](mailto:christoph.batik@bestit-online.at)

Lead-Developer: [Armin Neuhauser](mailto:neuhauser@bestit-online.at)

Wiki: [$PROJECTTITLE](http://example.com)

GIT Flow: [GIT Flow](https://www.atlassian.com/git/tutorials/comparing-workflows#gitflow-workflow)

Contribute: [Contribute.md](Contribute.md)

System: [Shopware 5](https://en.shopware.com)

(System) Architecture: [Architecture WIKI](http://example.com)

Credentials: [Credentials WIKI](http://example.com)

## Development Setup

### Technical requirements

#### Mac OS X

Make sure you have the following software installed:

- latest [PHPStorm](https://www.jetbrains.com/phpstorm/download/) (tested with v2017.2.4)
- latest [VirtualBox](https://www.virtualbox.org/wiki/Downloads) (tested with v5.1.28)
- latest [Sequal PRO](https://sequelpro.com/) (tested with v1.1.2)
- latest [SourceTree](https://www.sourcetreeapp.com/) (tested with v2.6.3)
- latest [Composer](https://getcomposer.org/download/) (tested with v1.5.2)
- latest [Vagrant](https://www.vagrantup.com/downloads.html) (tested with v2.0.0)

### Setting up Vagrant

- Open `terminal` and go to your `PHPStorm Projects` folder.
- Create a new folder `truckmo` using `$ mkdir truckmo`
- Inside `truckmo` run the following command: `$ git clone https://github.com/shopwareLabs/shopware-vagrant`
- Go inside `shopware-vagrant` and run `vagrant up`(it will take a while so be patient) 
- Once `Vagrant` is finished run: `vagrant ssh`
- Then: `$ cd www/` and if you see `adminer.php` and `phpinfo.php` then the installation was successful!
- Inside `www/` run `$ mkdir shopware-truckmo`. This is the folder where our truckmo shopware will be.
- Type `exit` to exit the ssh

#### Important

You can switch between `PHP 5.6/7.0/7.1`. For more information read the `README.md` inside the `shopware-vagrant` folder.
If you have used this set up method for previous shopware installations you will need to change `vb.name` inside the `Vagrant`file.

### Setting up TRUCKMO Shopware

- Open Terminal and go to your `PHPStorm Projects/truckmo` folder.
- Inside create a new folder called `shopware-truckmo`

    ![vagrant-setup](readme_img/shopware-truckmo.png#1)

- Inside `shopware-truckmo` run `$ git clone https://gdamyanov@bitbucket.org/best-it/truckmo.git`

    ![vagrant-setup](readme_img/shopware-truckmo2.png#1)

- After it is done your truckmo shopware project will be under `/truckmo/shopware-truckmo/truckmo/`

### PHPStorm SFTP with Vagrant

- Open the `truckmo` folder inside `PHPStorm`.

    ![phpstorm](readme_img/phpstorm-truckmo.png#1)
 
- Go to `Tools`->`Deployment`->`Configuration`

    ![phpstorm](readme_img/configuration.png#1)
 
- Add a new `SFTP` server with the following settings:
- Connection (TAB)
    - `Type`: `SFTP`
    - `SFTP host`: `192.168.33.10`
    - `Port`: `22`
    - `Root path`: `/home/vagrant/www/shopware-truckmo`
    - `User name`: `vagrant`
    - `Password`: `vagrant`
    - `Server root`:  will be filled automatically.
    
    ![phpstorm](readme_img/sftp1.png#1)

- Mappings (TAB)
    - `Deployment path on server`: `/`
      
    ![phpstorm](readme_img/sftp2.png#1)

- Now our `SFTP` server is configured. Let's open it by going to `Tools->Deployment->Browse Remote Host`

    ![phpstorm](readme_img/browseremotehost.png#1)
 
- Rightclick on our project and click `Upload to shopware-truckmo`. This step will upload our local `truckmo-shopware` to the server.

    ![phpstorm](readme_img/uploadtovagrant.png#1)
 
That is nice, however you don't want to do that every time you make a change. We can tell `PHPStorm` to upload the changes automatically on save.

### PHPStorm upload automatically

- Go to `Preferences->Deployment->Options` and change `Upload changed files automatically to the default server` to `Always`
    
    ![phpstorm](readme_img/uploadonsave.png#1)
 
- Go to `Tools->Deployment->Automatic Upload`
    
    ![phpstorm](readme_img/automaticupload.png#1)

Now every time you make a change and save it `PHPStorm` will upload it to the server.

### TRUCKMO DB Export

Let's first export the most recent DB from `truckmo`.

- Open `Sequal PRO` and create a new `SSH` connection:
    - The `MySQL` information you can find in your `LastPass`. Open your browser and click on the LastPass add-on, type `truckmo` and select the `MySQL`. (enter the host without the phpmyadmin-)
    - The `SSH` information you can find in your `LastPass`. Open your browser and click on the LastPass add-on, type `truckmo` and select `FTP/SSH`.
- Fill out the form and click `Test connection`.
- If the connection is successful click on `Add to Favorites`.

    ![vagrant-setup](readme_img/dbconnection.png#1)

- Now you can see a new favorite connection called `truckmo`. Connect to it with double click.
- Change the DB:

    ![vagrant-setup](readme_img/choosedb.png#1)  
    
- Go to `File->Export` and export the DB to your Desktop (or folder by your choice)

    ![vagrant-setup](readme_img/exportdb.png#1) 
    
- The file size is `400MB+`, so be patient.
- After it is done downloading it is time to import our new DB to our server.

### TRUCKMO DB Import

- Open `Sequal Pro` and create a new `SSH` connection:
    - The `MySQL` information is:
        - `Host`: `127.0.0.1`
        - `Username`: `root`
        - `Password`: `shopware`
    
    - The `SSH` information is:
        - `Host`: `192.168.33.10`
        - `User`: `vagrant`
        - `Password`: `vagrant`
- Add a new Db: `shopware-truckmo`

    ![db](readme_img/addb.png#1)
    
- Go to `File->Import` and import the truckmo Db.
- We have to do some changes in the Db so our local copy works properly.

### TRUCKMO change host in DB

- Connect to your local truckmo Db using `Sequal Pro`
- Go to `s_core_shops` and change the following:
    - `host`: `192.168.33.10`
    - `hosts`: `192.168.33.10`
    - `base_path`: `/shopware-truckmo`
    
    ![db](readme_img/hostchanged.png#1)

### TRUCKMO delete ViiSON-DHL Plugin

- Connect to your local truckmo Db using `Sequal Pro`
- Go to `s_core_plugins` and delete the `ViiSON Plugin` from the Db.

### Create config.php

`config.php` is not included in the repository, so we have to create one:

- Inside your project root folder create `config.php`
- Add the following:

```
<?php

return array (
    'db' =>
        array (
            'username' => 'root',
            'password' => 'shopware',
            'host' => 'localhost',
            'port' => '3306',
            'dbname' => 'shopware-truckmo',
        ),
    'csrfProtection' => [
        'frontend' => false,
        'backend' => false
    ],
);

```

### Create .htaccess

`.htaccess` is not include in the repository, so we have to create one:

- Inside your project root folder create `.htaccess`
- Add the following:

```
<IfModule mod_rewrite.c>
RewriteEngine on

#RewriteBase /shopware/

# Https config for the backend
#RewriteCond %{HTTPS} !=on
#RewriteRule backend/(.*) https://%{HTTP_HOST}%{REQUEST_URI} [L,R=301]

RewriteRule shopware.dll shopware.php
RewriteRule files/documents/.* engine [NC,L]
RewriteRule backend/media/(.*) media/$1 [NC,L]

RewriteCond %{REQUEST_URI} !(\/(engine|files|templates)\/)
RewriteCond %{REQUEST_URI} !(\/media\/(archive|banner|image|music|pdf|unknown|video)\/)
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ shopware.php [PT,L,QSA]

# Fix missing authorization-header on fast_cgi installations
RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
</IfModule>

# Staging-Rules start
#SetEnvIf Host "staging.test.shopware.in" ENV=staging

DirectoryIndex index.html
DirectoryIndex index.php
DirectoryIndex shopware.php

# Disables download of configuration
<Files ~ "\.(tpl|yml|ini)$">
    Deny from all
</Files>

# Enable gzip compression
<IfModule mod_deflate.c>
    AddOutputFilterByType DEFLATE text/html text/xml text/plain text/css text/javascript application/json
</IfModule>

<IfModule mod_expires.c>
<Files ~ "\.(jpe?g|png|gif|css|js)$">
    ExpiresActive on
    ExpiresDefault "access plus 1 month"
    FileETag None
    <IfModule mod_headers.c>
        Header append Cache-Control "public"
        Header unset ETag
    </IfModule>
</Files>
</IfModule>

# Disables auto directory index
<IfModule mod_autoindex.c>
	Options -Indexes
</IfModule>

<IfModule mod_negotiation.c>
    Options -MultiViews
</IfModule>

<IfModule mod_php5.c>
#  php_value memory_limit 128M
#  php_value max_execution_time 30
#  php_value upload_max_filesize 20M
   php_flag phar.readonly off
   php_flag magic_quotes_gpc off
   php_flag session.auto_start off
   php_flag suhosin.session.cryptua off
   php_flag zend.ze1_compatibility_mode off
</IfModule>

#   AddType x-mapp-php5 .php
#   AddHandler x-mapp-php5 .php

#<mittwald gzip and http-cache>
<IfModule mod_mime.c>

    AddType application/atom+xml                        atom
    AddType application/json                            json map topojson
    AddType application/ld+json                         jsonld
    AddType application/rss+xml                         rss
    AddType application/vnd.geo+json                    geojson
    AddType application/xml                             rdf xml

    AddType application/javascript                      js

    AddType application/manifest+json                   webmanifest
    AddType application/x-web-app-manifest+json         webapp
    AddType text/cache-manifest                         appcache

    AddType audio/mp4                                   f4a f4b m4a
    AddType audio/ogg                                   oga ogg opus
    AddType image/bmp                                   bmp
    AddType image/svg+xml                               svg svgz
    AddType image/webp                                  webp
    AddType video/mp4                                   f4v f4p m4v mp4
    AddType video/ogg                                   ogv
    AddType video/webm                                  webm
    AddType video/x-flv                                 flv

    AddType image/x-icon                                cur ico

    AddType application/font-woff                       woff
    AddType application/font-woff2                      woff2
    AddType application/vnd.ms-fontobject               eot

    AddType application/x-font-ttf                      ttc ttf
    AddType font/opentype                               otf

    AddType application/octet-stream                    safariextz
    AddType application/x-bb-appworld                   bbaw
    AddType application/x-chrome-extension              crx
    AddType application/x-opera-extension               oex
    AddType application/x-xpinstall                     xpi
    AddType text/vcard                                  vcard vcf
    AddType text/vnd.rim.location.xloc                  xloc
    AddType text/vtt                                    vtt
    AddType text/x-component                            htc

</IfModule>

<IfModule mod_deflate.c>
    AddOutputFilterByType DEFLATE "application/atom+xml" \
                                  "application/javascript" \
                                  "application/json" \
                                  "application/ld+json" \
                                  "application/manifest+json" \
                                  "application/rdf+xml" \
                                  "application/rss+xml" \
                                  "application/schema+json" \
                                  "application/vnd.geo+json" \
                                  "application/vnd.ms-fontobject" \
                                  "application/x-font-ttf" \
                                  "application/x-javascript" \
                                  "application/x-web-app-manifest+json" \
                                  "application/xhtml+xml" \
                                  "application/xml" \
                                  "font/eot" \
                                  "font/opentype" \
                                  "image/bmp" \
                                  "image/svg+xml" \
                                  "image/vnd.microsoft.icon" \
                                  "image/x-icon" \
                                  "text/cache-manifest" \
                                  "text/css" \
                                  "text/html" \
                                  "text/javascript" \
                                  "text/plain" \
                                  "text/vcard" \
                                  "text/vnd.rim.location.xloc" \
                                  "text/vtt" \
                                  "text/x-component" \
                                  "text/x-cross-domain-policy" \
                                  "text/xml"

</IfModule>

<IfModule mod_expires.c>

    ExpiresActive on
    ExpiresDefault                                      "access plus 1 month"

    ExpiresByType text/css                              "access plus 1 year"

    ExpiresByType application/atom+xml                  "access plus 1 hour"
    ExpiresByType application/rdf+xml                   "access plus 1 hour"
    ExpiresByType application/rss+xml                   "access plus 1 hour"

    ExpiresByType application/json                      "access plus 0 seconds"
    ExpiresByType application/ld+json                   "access plus 0 seconds"
    ExpiresByType application/schema+json               "access plus 0 seconds"
    ExpiresByType application/vnd.geo+json              "access plus 0 seconds"
    ExpiresByType application/xml                       "access plus 0 seconds"
    ExpiresByType text/xml                              "access plus 0 seconds"

    ExpiresByType image/vnd.microsoft.icon              "access plus 1 week"
    ExpiresByType image/x-icon                          "access plus 1 week"

    ExpiresByType text/html                             "access plus 0 seconds"

    ExpiresByType application/javascript                "access plus 1 year"
    ExpiresByType application/x-javascript              "access plus 1 year"
    ExpiresByType text/javascript                       "access plus 1 year"

    ExpiresByType application/manifest+json             "access plus 1 week"
    ExpiresByType application/x-web-app-manifest+json   "access plus 0 seconds"
    ExpiresByType text/cache-manifest                   "access plus 0 seconds"

    ExpiresByType audio/ogg                             "access plus 1 month"
    ExpiresByType image/bmp                             "access plus 1 month"
    ExpiresByType image/gif                             "access plus 1 month"
    ExpiresByType image/jpeg                            "access plus 1 month"
    ExpiresByType image/png                             "access plus 1 month"
    ExpiresByType image/svg+xml                         "access plus 1 month"
    ExpiresByType image/webp                            "access plus 1 month"
    ExpiresByType video/mp4                             "access plus 1 month"
    ExpiresByType video/ogg                             "access plus 1 month"
    ExpiresByType video/webm                            "access plus 1 month"

    ExpiresByType application/vnd.ms-fontobject         "access plus 1 month"
    ExpiresByType font/eot                              "access plus 1 month"

    ExpiresByType font/opentype                         "access plus 1 month"

    ExpiresByType application/x-font-ttf                "access plus 1 month"

    ExpiresByType application/font-woff                 "access plus 1 month"
    ExpiresByType application/x-font-woff               "access plus 1 month"
    ExpiresByType font/woff                             "access plus 1 month"

    ExpiresByType application/font-woff2                "access plus 1 month"

    ExpiresByType text/x-cross-domain-policy            "access plus 1 week"

</IfModule>
#</mittwald gzip and http-cache>

```

## (Software) Architecture 

Describe the main components of the software / system architecture:

- Components (Redis, ElesticSearch) not their configuration
- Flows and patterns
- Commands

## Testing

There are no tests at this point, you have to test manually.

## Build & Deployment

- To deploy the code use `SourceTree`
- `Commit` the changes
- Push them to the `dev` branch
- Merge with `master`

## Additional Documentation

### Troubleshooting

If the shop is not accessible after the setup check your apache error.log

- Open terminal and go to your `shopware-vagrant`
- Run the following commands:
```
    $ vagrant ssh    
    $ cd /var/log/apache2    
    $ tail -f error.log
```    
- Refresh your browser and the error message(if any) should appear inside the terminal.

If you get `Listener "registerViisonCommonSubscriber" in "ViisonDHL" is not callable.` you have to delete the `ViisonDHL` plugin in your local Db.

![db](readme_img/viisondhl.png#1)